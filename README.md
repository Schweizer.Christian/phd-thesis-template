## LaTeX template for a PhD thesis

Latex tamplate for PhD thesis based on `scrbook`.

## Hints

* Don't forget to add author and thesis title to `\hypersetup` in misc.tex.
* If you use the `\DeclareBibliographyCategory{cited}` part in misc.tex you can also print a bibliography of items in lib.bib but not cited.
* Compilation is much fster if you do not use `microtype` during writing.
